#include <ctime>
#include <chrono>
#include <sys/time.h>
#include <iostream>
#include <cmath>

using namespace std;
using namespace std::chrono;

long int current_time;
long int deltatime;
struct timeval tp;

void update_deltatime(){
    //get the time
    gettimeofday(&tp, NULL);
    long int ms = tp.tv_sec * 1000 + tp.tv_usec / 1000; //get current timestamp in milliseconds
    deltatime=ms-current_time;
    current_time=ms;
    //cout<<deltatime<<"\n";//DEBUG_TIME
    //30ms --> 0.03s
}
/*
int deltatime(){
    return(0);
}
*/
