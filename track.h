#include <iostream>
#include <cmath>

#include <opencv2/opencv.hpp>
#include <cxcore.hpp>

#include <fstream>

#include "geometria.h"
#include "gdeb.h"

using namespace cv;
using namespace std;

VideoCapture cap_a(0);
VideoCapture cap_b(2);

int aktualna_kamera=0;

class punkt{//punkt może przechowywać pozycje xyz lub kolor rgb
public:
  int x;
  int y;
  int z;
};

class funkt{//punkt może przechowywać pozycje xyz lub kolor rgb
public:
  float x;
  float y;
  float z;
};


class kamera{//klasa dla kazdej fizycznej kamery
public:
    //main
    int frame_width;
    int frame_height;
    Mat frame;
    
    //track
    punkt target[5];
    punkt track[5];
    geopunkt orjent[3];
    
    //debug
    int vision_range=10;
    bool cry_vision;
    
    Mat warstwa[5];
    Mat render;
    Mat first_frame;
    
    //obliczenia
    int r;
    int g;
    int b;
    int delta;
    int deltadwa;

    int xsuma;
    int ysuma;
    int liczbasuma;


    //wyniki
    float ori_x;
    float ori_y;
    
    //punkty wyznaczajace prosta
    funkt p_a;//wspuzendne_kamery_w_prawdziwym_swiecie
    funkt p_b;//sledzony_zutowany_na_plaszczyzne_odniesienia
    funkt plaszczak;//srodek plaszczyzny odniesienia
    
    void pixel(int x, int y, int num, int index){//funkcja sprawdzajaca na ile pixel jest bliski do wzorca
        r=frame.at<cv::Vec3b>(x,y)[2];
        g=frame.at<cv::Vec3b>(x,y)[1];
        b=frame.at<cv::Vec3b>(x,y)[0];
        delta=max(r-target[index].x, target[index].x-r)+max(g-target[index].y, target[index].y-g)+max(b-target[index].z, target[index].z-b);

        deltadwa=0;
        //delta+=100;
        for(int ii=0;ii<=2;ii++){
            deltadwa+=max(frame.at<cv::Vec3b>(x,y)[ii]-first_frame.at<cv::Vec3b>(x,y)[ii], first_frame.at<cv::Vec3b>(x,y)[ii]-frame.at<cv::Vec3b>(x,y)[ii]);
        }
        
        //deltadwa=delta;
        /*
        delta=((255*4)-deltadwa);
        //delta=delta/5;//DEBUG
        render.at<cv::Vec3b>(x,y)[2]=delta;
        render.at<cv::Vec3b>(x,y)[1]=delta;
        render.at<cv::Vec3b>(x,y)[0]=delta;
        */
        
        
        if(track[index].z>delta){//poprawiamy rekord
            track[index].z=delta;
            track[index].x=x;
            track[index].y=y;
        }
        
        if(delta<1){//tu parametr
            xsuma+=x;
            ysuma+=y;
            liczbasuma++;
            
            
            //DEBUG
            render.at<cv::Vec3b>(x,y)[2]=255;
            render.at<cv::Vec3b>(x,y)[1]=0;
            render.at<cv::Vec3b>(x,y)[0]=0;
        }else{//to jest bug specjalny
            render.at<cv::Vec3b>(x,y)[2]=frame.at<cv::Vec3b>(x,y)[2];
            render.at<cv::Vec3b>(x,y)[1]=frame.at<cv::Vec3b>(x,y)[1];
            render.at<cv::Vec3b>(x,y)[0]=frame.at<cv::Vec3b>(x,y)[0];
        }
        
    }
    
    void szybki_reset(int num){//musi co klatke zostac wywolany na kazdej kameze
        liczbasuma=0;
        xsuma=0;
        ysuma=0;
        track[0].z=10000000;
        render=frame;
    }
    
    void wylicz_wzglendne(){//wspuzendne sledzonego punktu na plaszczyznie orjentacyjnej
        //x
        //cout<<"X: "<<track[0].x<<"    Y: "<<track[0].y<<"\n";//DEBUG
        
        //cout<<"float("<<track[0].x<<"-"<<orjent[0].x<<")\n";//DEBUG
        //cout<<"ori_x="<<float(track[0].x-orjent[0].x)<<"/"<<float(orjent[1].x-orjent[0].x)<<"\n";//DEBUG
        //ori_x=float(track[0].x-orjent[0].x)/float(orjent[1].x-orjent[0].x);//bardzo niematematyczny kod
        //ori_y=float(track[0].y-orjent[2].y)/float(orjent[1].y-orjent[2].y);//bardzo niematematyczny kod
        
        //cout<<"x: "<<ori_x<<"\n";//<<"   y: "<<ori_y<<"\n";//DEBUG
        
        //ori_x=(0-(float(track[0].x)-(frame_height/2))/100);
        //ori_y=(float(track[0].y)-(frame_width/2))/100;
        
        //brdzo zly kod
        //int rdyst=1;
        //ori_x=(0-(float(track[0].x-orjent[1].x)))/float(orjent[1].x-orjent[1].x);//wysokosc
        ori_x=float(track[0].x-orjent[1].y)/float(orjent[2].y-orjent[1].y);//wysokosc
        //cout<<float(track[0].x-orjent[1].y)<<"/"<<float(orjent[2].y-orjent[1].y)<<"\n";//wysokoscDEBUG
        ori_y=float(track[0].y-orjent[1].x)/float(orjent[0].x-orjent[1].x);//szerokosc
        
        
        p_b=plaszczak;
        p_b.x+=ori_y;
        p_b.z+=ori_x;
    }
    
    
    bool init_camera(int num){//inicjalizacja kamery
        if(num==0){
            if(!cap_a.isOpened())
            {
                cerr <<"Brak strumienia z kamery["<<num<<"]"<< endl;//BLAD
                return(0);
            }
            cap_a >> render;
            cap_a >> first_frame;
            frame_width = cap_a.get(CV_CAP_PROP_FRAME_WIDTH);
            frame_height = cap_a.get(CV_CAP_PROP_FRAME_HEIGHT);
        }else{
            if(!cap_b.isOpened())
            {
                cerr <<"Brak strumienia z kamery["<<num<<"]"<< endl;//BLAD
                return(0);
            }
            cap_b >> render;
            cap_b >> first_frame;
            frame_width = cap_b.get(CV_CAP_PROP_FRAME_WIDTH);
            frame_height = cap_b.get(CV_CAP_PROP_FRAME_HEIGHT);
        }

        
        
        cout<<"frame_width: "<<frame_width<<"   frame_height"<<frame_height<<"\n";//DEBUG
        
        
        orjent[0].x=100;
        orjent[0].y=200;
        orjent[1].x=200;
        orjent[1].y=200;
        orjent[2].x=200;
        orjent[2].y=100;    
        
        
        //tylko do debuga  --- wszystkie dane bendo importowane z configa
        if(num==0){
            p_a.x=-0.5;
            p_a.y=-3;
            p_a.z=1;
        }else{
            p_a.x=0.5;
            p_a.y=-3;
            p_a.z=1;
        }
        p_b.x=1;
        p_b.y=1;
        p_b.z=1+num;
        
        plaszczak.x=0;
        plaszczak.y=2;
        plaszczak.z=1;
        
        return(1);
    }
    
    
    
};
kamera k[10];

/*
//matma
float skala(int orjent_a, int orjent_b, int sledzony){
    return((sledzony-orjent_a)/(orjent_b-orjent_a))
}*/


void brut(int num){//brutalne przeszukiwanie przestrzeni
    for (int i=0; i < k[num].frame.rows; ++i){
        for (int j=0; j < k[num].frame.cols; ++j){
            k[num].pixel(i, j, 0, 0);
        }
    }
}




//obsluga_orjentacji
void orjentacja(int num, int inp){//zaisanie nowej pozycji prostych orjetacyjnych
    inp--;
    k[num].orjent[inp].x=k[num].track[0].y;
    k[num].orjent[inp].y=k[num].track[0].x;
}

void new_tracker(int num){
  k[num].target[0].x=k[num].frame.at<cv::Vec3b>(100,100)[2];
  k[num].target[0].y=k[num].frame.at<cv::Vec3b>(100,100)[1];
  k[num].target[0].z=k[num].frame.at<cv::Vec3b>(100,100)[0];
  cout<<"done\n";//DEBUG
}




//zapisywanie
void szybki_zapis(int num){//zapisuje pozycje x y z -- to bendzie
    //k[num].track[0].x
    ofstream file;
    file.open ("blender/dane.txt");
    //file<<k[num].track[0].x-(k[num].frame_height/2)<<" "<<k[num].track[0].y-(k[num].frame_width/2)<<"\n";
    for(int i=0;i<2;i++){//proste_diagnostyczne
        //cout<<k[i].p_a.x<<" "<<k[i].p_a.y<<" "<<k[i].p_a.z<<" "<<k[i].p_b.x<<" "<<k[i].p_b.y<<" "<<k[i].p_b.z<<"\n";//DEBUG
        file<<k[i].p_a.x<<" "<<k[i].p_a.y<<" "<<k[i].p_a.z<<" "<<k[i].p_b.x<<" "<<k[i].p_b.y<<" "<<k[i].p_b.z<<" ";
    }
    file.close();
}
void duzy_zapis(){//update po zmianach w parametrach sledzenia
    int num=0;
    //k[0].orjent[inp].x
    ofstream file;
    file.open ("blender/orient.txt");
    for(int i=0;i<3;i++){
        file<<k[0].orjent[i].x-(k[num].frame_height/2)<<" "<<k[0].orjent[i].y-(k[num].frame_width/2)<<" ";
    }
    file.close();
}


//shadows  --  funkcje debugu graficznego
void kola_na_track(int num){
    circle(k[num].frame,Point(100, 100), 15, 255, 4);//static
  
    circle(k[num].frame,Point(k[num].track[0].y, k[num].track[0].x), 15, 255, 4);
    circle(k[num].render,Point(100, 100), 15, 255, 4);//static
  
    circle(k[num].render,Point(k[num].track[0].y, k[num].track[0].x), 15, 255, 4);

    //circle(k[num].render,Point(k[num].orjent[0].x, k[num].orjent[0].y), 60, 255, 20);//DEBUG
}

void proste_proste(int num){
    //cout<<"test\n";//DEBUG
    for(int i=0;i<2;i++){
        line(k[num].frame,Point(k[num].orjent[i].x, k[num].orjent[i].y), Point(k[num].orjent[i+1].x, k[num].orjent[i+1].y), 255, 4);//static
    }
}

void shadows(int num){
    //cout<<"In The Shadows\n";//DEBUG
    //circle(k[1].frame,Point(300, 300), 60, 255, 15);
    //k.[num].track[0].x;
    kola_na_track(num);
    proste_proste(num);
    
}


/*
//interfejsy
bool init_camera(int num){//inicjalizacja kamery
    if(num==0){
        if(!cap_a.isOpened())
        {
            cerr <<"Brak strumienia z kamery["<<num<<"]"<< endl;//BLAD
            return(0);
        }
        cap_a >> k[num].render;
        cap_a >> k[num].first_frame;
        k[num].frame_width = cap_a.get(CV_CAP_PROP_FRAME_WIDTH);
        k[num].frame_height = cap_a.get(CV_CAP_PROP_FRAME_HEIGHT);
    }else{
        if(!cap_b.isOpened())
        {
            cerr <<"Brak strumienia z kamery["<<num<<"]"<< endl;//BLAD
            return(0);
        }
        cap_b >> k[num].render;
        cap_b >> k[num].first_frame;
        k[num].frame_width = cap_b.get(CV_CAP_PROP_FRAME_WIDTH);
        k[num].frame_height = cap_b.get(CV_CAP_PROP_FRAME_HEIGHT);
    }

    
    
    cout<<"frame_width: "<<k[num].frame_width<<"   frame_height"<<k[num].frame_height<<"\n";//DEBUG
    
    
    k[num].orjent[0].x=100;
    k[num].orjent[0].y=200;
    k[num].orjent[1].x=200;
    k[num].orjent[1].y=200;
    k[num].orjent[2].x=200;
    k[num].orjent[2].y=100;    
    
    return(1);
}*/

void kill_them_all(){
    cap_a.release();
    cap_b.release();
    destroyAllWindows();
    cerr<<"dead\n";
}

bool read_camera(int num){
    if(num==0){
        cap_a >> k[num].frame;
        //cap_a >> k[num].render;
    }else{
        cap_b >> k[num].frame;
        //cap_b >> k[num].render;
    }
    //k[num].frame >> k[num].render;
    if (k[num].frame.empty()){
      cerr<<"Utracono Obraz\n";//BLAD
      return(0);
    }
    return(1);
}

void renderowanie(int num){
    //imshow( "Frame",k[num].frame );
    imshow( "Frame", k[num].render );
}



bool obsluga_klawiatury(){
    char c = (char)waitKey(1);
    int znak=(int)c;
    
    if(znak!=-1){//cokolwiek
      cout<<"znak["<<znak<<"]\n";
    }

    //wyjscie
    if( c == 27 ){//ESC
      return(0);
    }
    if( c == 'q' ){
      return(0);
    }
    
    //komendy
    if( c == 'c' ){
      //cry_vision=(!cry_vision);
    }
    if( znak == -85 ){//+
      cout<<"++\n";
      //vision_range++;
    }
    if( znak == -83 ){//-
      cout<<"--\n";
      //vision_range--;
    }
    if( c == 'o' ){
      aktualna_kamera=0;
    }
    if( c == 'p' ){
      aktualna_kamera=1;
    }
    
    
    //kalibracja
    if( c == 32 ){//SPACE
      new_tracker(aktualna_kamera);
    }
    
    if( znak == 49 ){//1
      cout<<"1\n";
      orjentacja(aktualna_kamera, 1);
      duzy_zapis();
    }
    if( znak == 50 ){//2
      cout<<"2\n";
      orjentacja(aktualna_kamera, 2);
      duzy_zapis();
    }
    if( znak == 51 ){//3
      cout<<"3\n";
      orjentacja(aktualna_kamera, 3);
      duzy_zapis();
    }
    
    return(1);
}
